Development notes
=================

Programming languages
---------------------

The code is written in C and Vala. The Vala code is progressively rewritten in
C.

Cleanliness of the code
-----------------------

I wrote the Vala code when I was a beginner, so some parts may be ugly and
not well implemented. The C code is much better implemented (in my opinion).

Old names still present internally
----------------------------------

The application was previously named LaTeXila and then GNOME LaTeX. The names
are sometimes still present in the code.

Gtex
----

The C code is grouped into Gtex, located in `src/gtex/`. It has GTK-Doc comments
with GObject Introspection annotations, to generate automatically a `*.gir` file
so Gtex can be used in the Vala code.

Gtex is an internal library statically linked to the executable. The Vala code
can use Gtex, but the reverse should be avoided. Although it is possible in C to
call Vala code, it's better to keep Gtex as a layer located below the
application, to avoid tight-coupling. That way it's also easier to move parts of
Gtex to another project, possibly a shared library.

LaTeX build tools
-----------------

A post-processor for Rubber was available, but has been removed. Rubber has some
bugs and Latexmk has more features. It would be possible to improve Rubber, fix
the bugs and re-add the post-processor.
