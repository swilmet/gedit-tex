Enter TeX - more information
============================

Getting in touch
----------------

- [Matrix](https://matrix.to/#/%23enter-tex:matrix.org)

Articles
--------

- [Some principles for the user experience of a LaTeX editor](https://gitlab.gnome.org/swilmet/enter-tex-extra/-/blob/main/articles/pdf/latex-editor-some-principles.pdf)

Dependencies
------------

Enter TeX depends on GTK 3.

Enter TeX is based on
[Gedit Technology](https://gedit-text-editor.org/technology.html):
- libgedit-amtk
- libgedit-gfls
- libgedit-gtksourceview
- libgedit-tepl
- [gspell](https://gitlab.gnome.org/GNOME/gspell)

Other dependencies:
- [gee-0.8](https://gitlab.gnome.org/GNOME/libgee)
- gsettings-desktop-schemas
- dconf (optional, recommended on Linux, can be disabled with
  `--disable-dconf-migration`)
- Latexmk >= 4.31 (optional, but strongly recommended)

Installation
------------

### Packages

To easily install Enter TeX, see if it has been packaged for your operating
system:
[list of packages on Repology](https://repology.org/project/gnome-latex/versions).

### Install procedure

Meson doesn't support well our use-case with C code mixed with Vala code, so
**the compilation is done with two ninja commands**.

```
$ cd build/
$ meson setup
$ ninja src/gtex/Gtex-1.gir
$ ninja
[ Become root if necessary ]
$ ninja install
```

Some links
----------

- [Project home page](https://gitlab.gnome.org/swilmet/enter-tex)
- [enter-tex-extra](https://gitlab.gnome.org/swilmet/enter-tex-extra) git
  repository
- [Old gnome-latex tarballs](https://download.gnome.org/sources/gnome-latex/)
- [Old latexila tarballs](https://download.gnome.org/sources/latexila/)
- [Old mailing list](https://mail.gnome.org/archives/latexila-list/)
