Enter TeX.next
==============

What would an Enter TeX.next project look like.

Or: the lessons learned.

Build tools
-----------

For the build tools, parsing the commands output is quite fragile, especially
the output of latexmk. And the latex post-processor is full of heuristics, some
warning/error messages are not extracted correctly. With retrospection, it was
a bad idea to implement it like that. To have a more robust implementation, it
would have been better to parse the log file and have a
[log viewer](https://gitlab.gnome.org/Archive/gnome-latex/-/issues/30).

In general for my other projects I now try to avoid implementing features that
have a fragile implementation (i.e. something not based on stable APIs, or with
heuristics, etc).

For Enter TeX, if the build tools break in the future, a solution is to create a
container with an older version of TeX Live bundled (or at least an older
version of latexmk bundled).

And, probably a lot of Enter TeX users anyway prefer to build the files outside
of the application, in a terminal. In that case, providing an embedded terminal
with the VTE library would be handy, with some TeX-specific features within the
terminal, like being able to click on a warning/error message to open the
corresponding file and line location.

AST
---

For the file structure in the side panel, a better implementation would be to
maintain an AST (Abstract Syntax Tree) attached to the GtkSourceBuffer, and by
using GtkSourceRegion. Then this AST can serve for other features, especially
for the auto-completion, or for text folding. For a project with multiple
`*.tex` files, the AST can be created for files not yet opened in the
application; this would be useful to provide completion for the `\ref`,
`\pageref` and `\cite` commands.

See also the Proposal 2
[here](https://wiki.gnome.org/Projects/GtkSourceView/CodeFolding).
