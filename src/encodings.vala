/* SPDX-FileCopyrightText: 2010 Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Encodings
{
    public const string[] CHARSETS =
    {
        // Encoding supported by default
        //"UTF-8",

        // European encoding
        "ISO-8859-1",
        "ISO-8859-2",
        "ISO-8859-3",
        "ISO-8859-4",
        "ISO-8859-5",
        "ISO-8859-7",
        "ISO-8859-9",
        "ISO-8859-10",
        "ISO-8859-13",
        "ISO-8859-14",
        "ISO-8859-15",
        "ISO-8859-16",

        // Unicode
        "UCS-2",
        "UCS-2BE",
        "UCS-2LE",
        "UCS-4",
        "UCS-4BE",
        "UCS-4LE",
        "UTF-16",
        "UTF-16BE",
        "UTF-16LE",
        "UTF-32",
        "UTF-32BE",
        "UTF-32LE",
        "UTF-7",

        // Semitic encoding
        "ISO-8859-6",
        "ISO-8859-8",
        "CP1255",
        "CP1256",
        "CP862",

        // Japanese encoding
        "EUC-JP",
        "SHIFT_JIS",
        "CP932",
        "ISO-2022-JP",
        "ISO-2022-JP-2",

        // Chinese enconding
        "EUC-CN",
        "GBK",
        "CP936",
        "GB18030",
        "EUC-TW",
        "BIG5",
        "CP950",
        "BIG5-HKSCS",
        "ISO-2022-CN",
        "ISO-2022-CN-EXT",

        // Korean encoding
        "EUC-KR",
        "CP949",
        "ISO-2022-KR",
        "JOHAB",

        // Armenian eccoding
        "ARMSCII-8",

        // Thai encoding
        "ISO-8859-11",

        // Windows encoding
        "WINDOWS-1250",
        "WINDOWS-1251",
        "WINDOWS-1252",
        "WINDOWS-1253",
        "WINDOWS-1254",
        "WINDOWS-1255",
        "WINDOWS-1256",
        "WINDOWS-1257",
        "WINDOWS-1258"
    };
}
