/* SPDX-FileCopyrightText: 2020 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTEX_TYPE_APP             (gtex_app_get_type ())
#define GTEX_APP(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_APP, GtexApp))
#define GTEX_APP_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_APP, GtexAppClass))
#define GTEX_IS_APP(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_APP))
#define GTEX_IS_APP_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_APP))
#define GTEX_APP_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_APP, GtexAppClass))

typedef struct _GtexApp         GtexApp;
typedef struct _GtexAppClass    GtexAppClass;
typedef struct _GtexAppPrivate  GtexAppPrivate;

struct _GtexApp
{
	GtkApplication parent;

	GtexAppPrivate *priv;
};

struct _GtexAppClass
{
	GtkApplicationClass parent_class;
};

GType		gtex_app_get_type	(void);

GtexApp *	gtex_app_new		(void);

G_END_DECLS
