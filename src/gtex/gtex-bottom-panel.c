/* SPDX-FileCopyrightText: 2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/**
 * SECTION:bottom-panel
 * @title: GtexBottomPanel
 * @short_description: The bottom panel
 *
 * Functions to create the bottom panel.
 */

#include "gtex-bottom-panel.h"
#include <glib/gi18n.h>
#include <tepl/tepl.h>

static void
close_button_clicked_cb (GtkButton *close_button,
			 GtkGrid   *hgrid)
{
	gtk_widget_hide (GTK_WIDGET (hgrid));
}

/**
 * gtex_bottom_panel_new:
 * @build_view: a #GtexBuildView.
 * @toolbar: the toolbar for @build_view.
 *
 * Returns: (transfer floating): the widget for the bottom panel.
 */
GtkWidget *
gtex_bottom_panel_new (GtexBuildView *build_view,
		       GtkToolbar    *toolbar)
{
	GtkGrid *hgrid;
	GtkGrid *vgrid;
	GtkWidget *scrolled_window;
	GtkWidget *close_button;

	g_return_val_if_fail (GTEX_IS_BUILD_VIEW (build_view), NULL);
	g_return_val_if_fail (GTK_IS_TOOLBAR (toolbar), NULL);

	/* Create widgets */

	hgrid = GTK_GRID (gtk_grid_new ());

	vgrid = GTK_GRID (gtk_grid_new ());
	gtk_orientable_set_orientation (GTK_ORIENTABLE (vgrid), GTK_ORIENTATION_VERTICAL);

	scrolled_window = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_set_hexpand (scrolled_window, TRUE);
	gtk_widget_set_vexpand (scrolled_window, TRUE);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolled_window),
					     GTK_SHADOW_IN);

	close_button = tepl_utils_create_close_button ();
	gtk_widget_set_tooltip_text (close_button, _("Hide panel"));
	g_object_set (close_button,
		      "margin", 3,
		      NULL);
	g_signal_connect_object (close_button,
				 "clicked",
				 G_CALLBACK (close_button_clicked_cb),
				 hgrid,
				 G_CONNECT_DEFAULT);

	gtk_widget_set_vexpand (GTK_WIDGET (toolbar), TRUE);

	/* Packing */

	gtk_container_add (GTK_CONTAINER (scrolled_window), GTK_WIDGET (build_view));

	gtk_container_add (GTK_CONTAINER (vgrid), close_button);
	gtk_container_add (GTK_CONTAINER (vgrid), GTK_WIDGET (toolbar));

	gtk_container_add (GTK_CONTAINER (hgrid), scrolled_window);
	gtk_container_add (GTK_CONTAINER (hgrid), GTK_WIDGET (vgrid));

	gtk_widget_show_all (scrolled_window);
	gtk_widget_show_all (GTK_WIDGET (vgrid));

	return GTK_WIDGET (hgrid);
}
