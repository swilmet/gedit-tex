/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include "gtex-build-view.h"
#include "gtex-post-processor.h"

G_BEGIN_DECLS

#define GTEX_TYPE_BUILD_JOB             (gtex_build_job_get_type ())
#define GTEX_BUILD_JOB(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_BUILD_JOB, GtexBuildJob))
#define GTEX_BUILD_JOB_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_BUILD_JOB, GtexBuildJobClass))
#define GTEX_IS_BUILD_JOB(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_BUILD_JOB))
#define GTEX_IS_BUILD_JOB_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_BUILD_JOB))
#define GTEX_BUILD_JOB_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_BUILD_JOB, GtexBuildJobClass))

typedef struct _GtexBuildJob        GtexBuildJob;
typedef struct _GtexBuildJobClass   GtexBuildJobClass;
typedef struct _GtexBuildJobPrivate GtexBuildJobPrivate;

struct _GtexBuildJob
{
  GObject parent;

  GtexBuildJobPrivate *priv;
};

struct _GtexBuildJobClass
{
  GObjectClass parent_class;
};

GType		gtex_build_job_get_type		(void);

GtexBuildJob *	gtex_build_job_new		(void);

GtexBuildJob *	gtex_build_job_clone		(GtexBuildJob *build_job);

gchar *		gtex_build_job_to_xml		(GtexBuildJob *build_job);

void		gtex_build_job_run_async	(GtexBuildJob        *build_job,
						 GFile               *file,
						 GtexBuildView       *build_view,
						 GCancellable        *cancellable,
						 GAsyncReadyCallback  callback,
						 gpointer             user_data);

gboolean	gtex_build_job_run_finish	(GtexBuildJob *build_job,
						 GAsyncResult *result);

G_END_DECLS
