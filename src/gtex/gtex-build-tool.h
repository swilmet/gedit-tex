/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include "gtex-build-job.h"
#include "gtex-build-view.h"

G_BEGIN_DECLS

#define GTEX_TYPE_BUILD_TOOL             (gtex_build_tool_get_type ())
#define GTEX_BUILD_TOOL(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_BUILD_TOOL, GtexBuildTool))
#define GTEX_BUILD_TOOL_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_BUILD_TOOL, GtexBuildToolClass))
#define GTEX_IS_BUILD_TOOL(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_BUILD_TOOL))
#define GTEX_IS_BUILD_TOOL_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_BUILD_TOOL))
#define GTEX_BUILD_TOOL_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_BUILD_TOOL, GtexBuildToolClass))

typedef struct _GtexBuildTool        GtexBuildTool;
typedef struct _GtexBuildToolClass   GtexBuildToolClass;
typedef struct _GtexBuildToolPrivate GtexBuildToolPrivate;

struct _GtexBuildTool
{
  GObject parent;

  GtexBuildToolPrivate *priv;
};

struct _GtexBuildToolClass
{
  GObjectClass parent_class;
};

GType		gtex_build_tool_get_type		(void);

GtexBuildTool *	gtex_build_tool_new			(void);

GtexBuildTool *	gtex_build_tool_clone			(GtexBuildTool *build_tool);

const gchar *	gtex_build_tool_get_description		(GtexBuildTool *build_tool);

void		gtex_build_tool_add_job			(GtexBuildTool *build_tool,
							 GtexBuildJob  *build_job);

GList *		gtex_build_tool_get_jobs		(GtexBuildTool *build_tool);

gchar *		gtex_build_tool_to_xml			(GtexBuildTool *tool);

void		gtex_build_tool_run_async		(GtexBuildTool       *build_tool,
							 GFile               *file,
							 GtexBuildView       *build_view,
							 GCancellable        *cancellable,
							 GAsyncReadyCallback  callback,
							 gpointer             user_data);

void		gtex_build_tool_run_finish		(GtexBuildTool *build_tool,
							 GAsyncResult  *result);

G_END_DECLS
