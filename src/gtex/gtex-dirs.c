/* SPDX-FileCopyrightText: 2008 - Ignacio Casal Quinteiro
 * SPDX-FileCopyrightText: 2022 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* Some code adapted from gedit-dirs.c. */

#include "config.h"
#include "gtex-dirs.h"

#ifdef OS_OSX
#include <gtkosxapplication.h>
#endif

/**
 * SECTION:dirs
 * @title: GtexDirs
 * @short_description: Application directories
 *
 * Simple functions to get some application directories. They depend on the OS.
 *
 * gtex_init() must be called before using the functions here.
 */

static gchar *app_data_dir = NULL;
static gchar *app_locale_dir = NULL;

/*
 * _gtex_dirs_init:
 *
 * This function must be called before starting the application.
 */
void
_gtex_dirs_init (void)
{
	static gboolean done = FALSE;

	if (done)
	{
		return;
	}

#if defined G_OS_WIN32
	{
		gchar *win32_dir;

		win32_dir = g_win32_get_package_installation_directory_of_module (NULL);

		if (win32_dir != NULL)
		{
			app_data_dir = g_build_filename (win32_dir, "share", "enter-tex", NULL);
			app_locale_dir = g_build_filename (win32_dir, "share", "locale", NULL);

			done = TRUE;
			g_free (win32_dir);
		}
	}
#elif defined OS_OSX
	if (gtkosx_application_get_bundle_id () != NULL)
	{
		const gchar *bundle_resource_dir = gtkosx_application_get_resource_path ();

		app_data_dir = g_build_filename (bundle_resource_dir, "share", "enter-tex", NULL);
		app_locale_dir = g_build_filename (bundle_resource_dir, "share", "locale", NULL);

		done = TRUE;
	}
#endif

	if (!done)
	{
		app_data_dir = g_strdup (DATA_DIR);
		app_locale_dir = g_strdup (LOCALE_DIR);
		done = TRUE;
	}
}

/*
 * _gtex_dirs_shutdown:
 *
 * This function must be called before exiting the application.
 */
void
_gtex_dirs_shutdown (void)
{
	g_clear_pointer (&app_data_dir, g_free);
	g_clear_pointer (&app_locale_dir, g_free);
}

/**
 * gtex_dirs_get_app_data_dir:
 *
 * For example on Unix/Linux, it's usually “${prefix}/share/enter-tex”.
 *
 * Returns: the directory where the data of this application is located.
 */
const gchar *
gtex_dirs_get_app_data_dir (void)
{
	return app_data_dir;
}

/**
 * gtex_dirs_get_app_locale_dir:
 *
 * For example on Unix/Linux, it's usually “${prefix}/share/locale”.
 *
 * Returns: the directory where locale data is located.
 */
const gchar *
gtex_dirs_get_app_locale_dir (void)
{
	return app_locale_dir;
}
