/* SPDX-FileCopyrightText: 2020 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <tepl/tepl.h>

G_BEGIN_DECLS

#define GTEX_TYPE_FACTORY             (gtex_factory_get_type ())
#define GTEX_FACTORY(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_FACTORY, GtexFactory))
#define GTEX_FACTORY_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_FACTORY, GtexFactoryClass))
#define GTEX_IS_FACTORY(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_FACTORY))
#define GTEX_IS_FACTORY_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_FACTORY))
#define GTEX_FACTORY_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_FACTORY, GtexFactoryClass))

typedef struct _GtexFactory         GtexFactory;
typedef struct _GtexFactoryClass    GtexFactoryClass;

struct _GtexFactory
{
	TeplAbstractFactory parent;
};

struct _GtexFactoryClass
{
	TeplAbstractFactoryClass parent_class;
};

GType		gtex_factory_get_type	(void);

GtexFactory *	gtex_factory_new	(void);

G_END_DECLS
