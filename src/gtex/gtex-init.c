/* SPDX-FileCopyrightText: 2020-2022 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"
#include "gtex-init.h"
#include <locale.h>
#include <libintl.h>
#include <tepl/tepl.h>
#include "gtex-dirs.h"
#include "gtex-settings.h"

/**
 * SECTION:init
 * @title: Gtex Initialization and Finalization
 */

static void
init_i18n (void)
{
	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, gtex_dirs_get_app_locale_dir ());
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
}

/**
 * gtex_init:
 *
 * Initializes the application (e.g. for the internationalization).
 *
 * This function can be called several times, but is meant to be called at the
 * beginning of main(), before any other Tepl function call.
 *
 * This function also calls tepl_init().
 */
void
gtex_init (void)
{
	static gboolean done = FALSE;

	if (!done)
	{
		_gtex_dirs_init ();
		init_i18n ();
		tepl_init ();

		done = TRUE;
	}
}

/**
 * gtex_finalize:
 *
 * Free the resources allocated by this application. For example it unrefs the
 * singleton objects.
 *
 * This function also calls tepl_finalize().
 *
 * It is not mandatory to call this function, it's just to be friendlier to
 * memory debugging tools. This function is meant to be called at the end of
 * main(). It can be called several times.
 */
void
gtex_finalize (void)
{
	static gboolean done = FALSE;

	if (!done)
	{
		_gtex_settings_unref_singleton ();
		tepl_finalize ();
		_gtex_dirs_shutdown ();

		done = TRUE;
	}
}
