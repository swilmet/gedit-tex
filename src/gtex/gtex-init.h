/* SPDX-FileCopyrightText: 2020 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

void	gtex_init	(void);

void	gtex_finalize	(void);

G_END_DECLS
