/* SPDX-FileCopyrightText: 2014 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "gtex-post-processor.h"

G_BEGIN_DECLS

#define GTEX_TYPE_POST_PROCESSOR_ALL_OUTPUT             (gtex_post_processor_all_output_get_type ())
#define GTEX_POST_PROCESSOR_ALL_OUTPUT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_POST_PROCESSOR_ALL_OUTPUT, GtexPostProcessorAllOutput))
#define GTEX_POST_PROCESSOR_ALL_OUTPUT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_POST_PROCESSOR_ALL_OUTPUT, GtexPostProcessorAllOutputClass))
#define GTEX_IS_POST_PROCESSOR_ALL_OUTPUT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_POST_PROCESSOR_ALL_OUTPUT))
#define GTEX_IS_POST_PROCESSOR_ALL_OUTPUT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_POST_PROCESSOR_ALL_OUTPUT))
#define GTEX_POST_PROCESSOR_ALL_OUTPUT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_POST_PROCESSOR_ALL_OUTPUT, GtexPostProcessorAllOutputClass))

typedef struct _GtexPostProcessorAllOutput        GtexPostProcessorAllOutput;
typedef struct _GtexPostProcessorAllOutputClass   GtexPostProcessorAllOutputClass;
typedef struct _GtexPostProcessorAllOutputPrivate GtexPostProcessorAllOutputPrivate;

struct _GtexPostProcessorAllOutput
{
	GtexPostProcessor parent;

	GtexPostProcessorAllOutputPrivate *priv;
};

struct _GtexPostProcessorAllOutputClass
{
	GtexPostProcessorClass parent_class;
};

GType			gtex_post_processor_all_output_get_type	(void);

GtexPostProcessor *	gtex_post_processor_all_output_new	(void);

G_END_DECLS
