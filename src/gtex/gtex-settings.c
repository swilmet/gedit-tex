/* SPDX-FileCopyrightText: 2020, 2024 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gtex-settings.h"
#include <tepl/tepl.h>

/**
 * SECTION:settings
 * @title: GtexSettings
 * @short_description: Singleton class containing #GSettings objects
 *
 * Singleton class containing #GSettings objects.
 *
 * See also #TeplSettings.
 */

struct _GtexSettingsPrivate
{
	GSettings *settings_editor;
	GSettings *settings_latex;
};

/* GtexSettings is a singleton. */
static GtexSettings *singleton = NULL;

G_DEFINE_TYPE_WITH_PRIVATE (GtexSettings, gtex_settings, G_TYPE_OBJECT)

static void
gtex_settings_dispose (GObject *object)
{
	GtexSettings *self = GTEX_SETTINGS (object);

	g_clear_object (&self->priv->settings_editor);
	g_clear_object (&self->priv->settings_latex);

	G_OBJECT_CLASS (gtex_settings_parent_class)->dispose (object);
}

static void
gtex_settings_finalize (GObject *object)
{
	if (singleton == GTEX_SETTINGS (object))
	{
		singleton = NULL;
	}

	G_OBJECT_CLASS (gtex_settings_parent_class)->finalize (object);
}

static void
gtex_settings_class_init (GtexSettingsClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->dispose = gtex_settings_dispose;
	object_class->finalize = gtex_settings_finalize;
}

static void
gtex_settings_init (GtexSettings *self)
{
	self->priv = gtex_settings_get_instance_private (self);

	self->priv->settings_editor = g_settings_new ("org.gnome.enter_tex.preferences.editor");
	self->priv->settings_latex = g_settings_new ("org.gnome.enter_tex.preferences.latex");
}

/**
 * gtex_settings_get_singleton:
 *
 * Returns: (transfer none): the #GtexSettings singleton instance.
 */
GtexSettings *
gtex_settings_get_singleton (void)
{
	if (singleton == NULL)
	{
		singleton = g_object_new (GTEX_TYPE_SETTINGS, NULL);
	}

	return singleton;
}

void
_gtex_settings_unref_singleton (void)
{
	if (singleton != NULL)
	{
		g_object_unref (singleton);
	}

	/* singleton is not set to NULL here, it is set to NULL in
	 * gtex_settings_finalize() (i.e. when we are sure that the ref
	 * count reaches 0).
	 */
}

void
_gtex_settings_setup (GtexSettings *self)
{
	TeplSettings *tepl_settings;

	g_return_if_fail (GTEX_IS_SETTINGS (self));

	tepl_settings = tepl_settings_get_singleton ();
	tepl_settings_provide_font_settings (tepl_settings,
					     self->priv->settings_editor,
					     "use-default-font",
					     "editor-font");

	tepl_settings_provide_style_scheme_settings (tepl_settings,
						     self->priv->settings_editor,
						     "style-scheme-for-light-theme-variant",
						     "style-scheme-for-dark-theme-variant");
}

/**
 * gtex_settings_peek_editor_settings:
 * @self: the #GtexSettings instance.
 *
 * Returns: (transfer none): the #GSettings for
 * `"org.gnome.enter_tex.preferences.editor"`.
 */
GSettings *
gtex_settings_peek_editor_settings (GtexSettings *self)
{
	g_return_val_if_fail (GTEX_IS_SETTINGS (self), NULL);
	return self->priv->settings_editor;
}

/**
 * gtex_settings_peek_latex_settings:
 * @self: the #GtexSettings instance.
 *
 * Returns: (transfer none): the #GSettings for
 * `"org.gnome.enter_tex.preferences.latex"`.
 */
GSettings *
gtex_settings_peek_latex_settings (GtexSettings *self)
{
	g_return_val_if_fail (GTEX_IS_SETTINGS (self), NULL);
	return self->priv->settings_latex;
}
