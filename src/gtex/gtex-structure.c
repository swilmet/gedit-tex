/* SPDX-FileCopyrightText: 2024 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "gtex-structure.h"
#include <glib/gi18n.h>

/**
 * SECTION:structure
 * @title: GtexStructure
 * @short_description: Document structure widget
 *
 * WIP, to implement.
 */

struct _GtexStructurePrivate
{
	gint something;
};

G_DEFINE_TYPE_WITH_PRIVATE (GtexStructure, gtex_structure, GTK_TYPE_GRID)

static void
gtex_structure_finalize (GObject *object)
{

	G_OBJECT_CLASS (gtex_structure_parent_class)->finalize (object);
}

static void
gtex_structure_class_init (GtexStructureClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize = gtex_structure_finalize;
}

static void
gtex_structure_init (GtexStructure *structure)
{
	structure->priv = gtex_structure_get_instance_private (structure);

	gtk_container_add (GTK_CONTAINER (structure),
			   gtk_label_new (_("To implement")));

	gtk_widget_show_all (GTK_WIDGET (structure));
}

/**
 * gtex_structure_new:
 *
 * Returns: (transfer floating): a new #GtexStructure widget.
 */
GtexStructure *
gtex_structure_new (void)
{
	return g_object_new (GTEX_TYPE_STRUCTURE, NULL);
}
