/* SPDX-FileCopyrightText: 2015 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTEX_TYPE_TEMPLATES_DEFAULT gtex_templates_default_get_type ()
G_DECLARE_FINAL_TYPE (GtexTemplatesDefault, gtex_templates_default, GTEX, TEMPLATES_DEFAULT, GtkListStore)

GtexTemplatesDefault *	gtex_templates_default_get_instance	(void);

gchar *			gtex_templates_default_get_contents	(GtexTemplatesDefault *templates,
								 GtkTreePath          *path);

G_END_DECLS
