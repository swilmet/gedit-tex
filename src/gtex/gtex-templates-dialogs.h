/* SPDX-FileCopyrightText: 2015 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

gchar *	gtex_templates_dialogs_open		(GtkWindow *parent_window);

void	gtex_templates_dialogs_create_template	(GtkWindow   *parent_window,
						 const gchar *template_contents);

G_END_DECLS
