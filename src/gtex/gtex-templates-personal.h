/* SPDX-FileCopyrightText: 2015 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTEX_TYPE_TEMPLATES_PERSONAL gtex_templates_personal_get_type ()
G_DECLARE_FINAL_TYPE (GtexTemplatesPersonal, gtex_templates_personal,
		      GTEX, TEMPLATES_PERSONAL,
		      GtkListStore)

GtexTemplatesPersonal *
		gtex_templates_personal_get_instance	(void);

gchar *		gtex_templates_personal_get_contents	(GtexTemplatesPersonal *templates,
							 GtkTreePath           *path);

gboolean	gtex_templates_personal_create		(GtexTemplatesPersonal  *templates,
							 const gchar            *name,
							 const gchar            *config_icon_name,
							 const gchar            *contents,
							 GError                **error);

gboolean	gtex_templates_personal_delete		(GtexTemplatesPersonal  *templates,
							 GtkTreeIter            *iter,
							 GError                **error);

gboolean	gtex_templates_personal_move_up		(GtexTemplatesPersonal  *templates,
							 GtkTreeIter            *iter,
							 GError                **error);

gboolean	gtex_templates_personal_move_down	(GtexTemplatesPersonal  *templates,
							 GtkTreeIter            *iter,
							 GError                **error);

G_END_DECLS
