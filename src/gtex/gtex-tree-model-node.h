/* SPDX-FileCopyrightText: 2024-2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GTEX_TYPE_TREE_MODEL_NODE             (gtex_tree_model_node_get_type ())
#define GTEX_TREE_MODEL_NODE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), GTEX_TYPE_TREE_MODEL_NODE, GtexTreeModelNode))
#define GTEX_TREE_MODEL_NODE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), GTEX_TYPE_TREE_MODEL_NODE, GtexTreeModelNodeClass))
#define GTEX_IS_TREE_MODEL_NODE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GTEX_TYPE_TREE_MODEL_NODE))
#define GTEX_IS_TREE_MODEL_NODE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), GTEX_TYPE_TREE_MODEL_NODE))
#define GTEX_TREE_MODEL_NODE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GTEX_TYPE_TREE_MODEL_NODE, GtexTreeModelNodeClass))

typedef struct _GtexTreeModelNode         GtexTreeModelNode;
typedef struct _GtexTreeModelNodeClass    GtexTreeModelNodeClass;
typedef struct _GtexTreeModelNodePrivate  GtexTreeModelNodePrivate;

struct _GtexTreeModelNode
{
	GObject parent;

	GtexTreeModelNodePrivate *priv;
};

struct _GtexTreeModelNodeClass
{
	GObjectClass parent_class;
};

GType		gtex_tree_model_node_get_type		(void);

void		gtex_tree_model_node_set_column_types	(GtexTreeModelNode *gtex_model,
							 gint               n_columns,
							 GType             *column_types);

void		gtex_tree_model_node_set_tree		(GtexTreeModelNode *model,
							 GNode             *tree);

gboolean	gtex_tree_model_node_column_index_is_valid	(GtexTreeModelNode *model,
								 gint               index);

void		gtex_tree_model_node_change_stamp	(GtexTreeModelNode *model);

void		gtex_tree_model_node_get_iter_at_node	(GtexTreeModelNode *model,
							 GtkTreeIter       *iter,
							 GNode             *node);

GNode *		gtex_tree_model_node_get_node_from_iter	(const GtkTreeIter *iter);

gboolean	gtex_tree_model_node_iter_is_valid	(GtexTreeModelNode *model,
							 const GtkTreeIter *iter);

G_END_DECLS
