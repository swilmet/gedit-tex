/* SPDX-FileCopyrightText: 2014-2025 - Sébastien Wilmet
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* String functions */

gboolean	gtex_utils_char_is_escaped	(const gchar *str,
						 gsize        char_index);

/* File-related functions */

gchar *		gtex_utils_get_relative_path	(GFile    *origin,
						 gboolean  origin_is_a_directory,
						 GFile    *target);

/* Widget-related functions */

void		gtex_utils_show_uri		(GtkWidget    *widget,
						 const gchar  *uri,
						 guint32       timestamp,
						 GError      **error);

GtkWidget *	gtex_utils_get_dialog_component	(const gchar *title,
						 GtkWidget   *widget);

/* Other */

void		gtex_utils_migrate_settings	(void);

G_END_DECLS
