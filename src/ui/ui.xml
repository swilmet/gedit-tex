<!--
SPDX-FileCopyrightText: 2010-2025 Sébastien Wilmet
SPDX-License-Identifier: GPL-3.0-or-later
-->

<ui>
  <menubar name="MainMenu">
    <menu action="File">
      <menuitem action="FileNew" />
      <menuitem action="FileNewWindow" />
      <menuitem action="FileOpen" />
      <menuitem action="FileOpenRecent" />
      <separator />
      <menuitem action="FileSave" />
      <menuitem action="FileSaveAs" />
      <separator />
      <menuitem action="FileCreateTemplate" />
      <menuitem action="FileManageTemplates" />
      <separator />
      <menuitem action="FileClose" />
      <menuitem action="FileQuit" />
    </menu>

    <menu action="Edit"></menu>

    <menu action="View">
      <menuitem action="ViewMainToolbar" />
      <menuitem action="ViewEditToolbar" />
      <menuitem action="ViewSidePanel" />
      <menuitem action="ViewBottomPanel" />
      <separator />
      <menuitem action="ViewZoomIn" />
      <menuitem action="ViewZoomOut" />
      <menuitem action="ViewZoomReset" />
    </menu>

    <menu action="Search">
      <menuitem action="SearchFind" />
      <menuitem action="SearchReplace" />
      <separator />
      <menuitem action="SearchGoToLine" />
      <separator />
      <menuitem action="SearchForward" />
    </menu>

    <menu action="Build" name="BuildMenu">
      <placeholder name="BuildToolsPlaceholderMenu" />
      <separator />
      <menuitem action="BuildClean" />
      <menuitem action="BuildStopExecution" />
      <menuitem action="BuildViewLog" />
      <separator />
      <menuitem action="BuildShowDetails" />
      <menuitem action="BuildShowWarnings" />
      <menuitem action="BuildShowBadBoxes" />
      <separator />
      <menuitem action="BuildToolsPreferences" />
    </menu>

    <menu action="Latex"></menu>
    <menu action="Math"></menu>

    <menu action="Documents" name="DocumentsMenu">
      <menuitem action="DocumentsSaveAll" />
      <menuitem action="DocumentsCloseAll" />
      <separator />
      <menuitem action="DocumentsPrevious" />
      <menuitem action="DocumentsNext" />
      <separator />
      <menuitem action="DocumentsMoveToNewWindow" />
      <placeholder name="DocumentsListPlaceholder">
      <separator />
      </placeholder>
    </menu>

    <menu action="Projects">
      <menuitem action="ProjectsNew" />
      <menuitem action="ProjectsConfigCurrent" />
      <menuitem action="ProjectsManage" />
    </menu>

    <menu action="Tools">
      <menuitem action="ToolsSpellCheckerDialog" />
      <menuitem action="ToolsSetSpellLanguage" />
      <menuitem action="ToolsInlineSpellChecker" />
    </menu>

    <menu action="Structure">
      <menuitem action="StructureCut" />
      <menuitem action="StructureCopy" />
      <menuitem action="StructureDelete" />
      <separator />
      <menuitem action="StructureSelect" />
      <separator />
      <menuitem action="StructureComment" />
      <separator />
      <menuitem action="StructureShiftLeft" />
      <menuitem action="StructureShiftRight" />
      <separator />
      <menuitem action="StructureOpenFile" />
    </menu>

    <menu action="Help">
      <menuitem action="HelpContents" />
      <menuitem action="HelpLatexReference" />
      <menuitem action="HelpAbout" />
    </menu>
  </menubar>

  <toolbar name="MainToolbar">
    <toolitem action="FileNew" />
    <toolitem action="FileSave" />
    <separator />
    <toolitem action="EditUndo" />
    <toolitem action="EditRedo" />
    <separator />
    <toolitem action="EditCut" />
    <toolitem action="EditCopy" />
    <toolitem action="EditPaste" />
    <separator />
    <toolitem action="SearchFind" />
    <toolitem action="SearchReplace" />
    <separator />
    <toolitem action="BuildClean" />
    <placeholder name="BuildToolsPlaceholderToolbar" />
  </toolbar>

  <toolbar name="BuildToolbar">
    <toolitem action="BuildStopExecution" />
    <toolitem action="BuildViewLog" />
    <toolitem action="BuildShowDetails" />
    <toolitem action="BuildShowWarnings" />
    <toolitem action="BuildShowBadBoxes" />
  </toolbar>

  <popup name="NotebookPopup">
    <menuitem action="DocumentsMoveToNewWindow" />
    <separator />
    <menuitem action="FileSave" />
    <menuitem action="FileSaveAs" />
    <separator />
    <menuitem action="FileClose" />
  </popup>

  <popup action="StructurePopup">
    <menuitem action="StructureCut" />
    <menuitem action="StructureCopy" />
    <menuitem action="StructureDelete" />
    <separator />
    <menuitem action="StructureSelect" />
    <separator />
    <menuitem action="StructureComment" />
    <separator />
    <menuitem action="StructureShiftLeft" />
    <menuitem action="StructureShiftRight" />
    <separator />
    <menuitem action="StructureOpenFile" />
  </popup>
</ui>
